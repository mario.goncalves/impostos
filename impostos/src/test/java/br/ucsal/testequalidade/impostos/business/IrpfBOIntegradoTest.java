package br.ucsal.testequalidade.impostos.business;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import br.ucsal.testequalidade.impostos.domain.FichaFinanceira;
import br.ucsal.testequalidade.impostos.domain.Funcionario;
import br.ucsal.testequalidade.impostos.persistence.FuncionarioDAO;
import br.ucsal.testequalidade.impostos.persistence.FichaFinanceiraDAO;
import builders.FichaFinanceiraBuilder;
import builders.FuncionarioBuilder;


public class IrpfBOIntegradoTest {
	@Test
	public void testarImpostoDeRenda() {
	FuncionarioBuilder funcionarioBuilder = FuncionarioBuilder.umFuncionario().comMatricula(1234).nome("Jo�o Guilherme").endereco("S�o Paulo").eEmail("joao12@gmail.com").nascidoEm(LocalDate.of(2001, 07, 13)).ePortadorOuNaoDeDoencasGraves(false);
	Funcionario funcionario1 = funcionarioBuilder.build();
	FichaFinanceiraBuilder fichaFinanceiraBuilder = FichaFinanceiraBuilder.umaFichaFinanceira().doFuncionario(funcionario1).comMesAnoDeReferencia(LocalDate.now()).dataDePagamentoEm(LocalDate.now()).comSalarioBrutoDe(new BigDecimal("2800.00")).eIrpfFinalDe(new BigDecimal("67.20"));
	FichaFinanceira ficha1 = fichaFinanceiraBuilder.build();
	
	FuncionarioDAO.insert(funcionario1);
	FichaFinanceiraDAO.insert(ficha1);
	
	BigDecimal ImpostoAtual = IrpfBO.calcularIr(ficha1);
	BigDecimal ImpostoEsperado = new BigDecimal("67.20");
	Assertions.assertEquals(ImpostoEsperado, ImpostoAtual);
	}
}
