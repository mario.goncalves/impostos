package builders;

import java.math.BigDecimal;
import java.time.LocalDate;

import br.ucsal.testequalidade.impostos.domain.FichaFinanceira;
import br.ucsal.testequalidade.impostos.domain.Funcionario;


public class FichaFinanceiraBuilder {

	private static final Funcionario FUNCIONARIO_DEFAULT = new Funcionario();
	private static final LocalDate MES_ANO_REFERENCIA_DEFAULT = LocalDate.now() ;
	private static final LocalDate DATA_PAGAMENTO_DEFAULT = LocalDate.now();
	private static final BigDecimal SALARIO_BRUTO_DEFAULT = new BigDecimal("2800.00");
	private static final BigDecimal IRPF_DEFAULT = new BigDecimal("67.20");

	private Funcionario funcionario = FUNCIONARIO_DEFAULT;
	private LocalDate mesAnoReferencia =MES_ANO_REFERENCIA_DEFAULT;
	private LocalDate dataPagamento = DATA_PAGAMENTO_DEFAULT;
	private BigDecimal salarioBruto = SALARIO_BRUTO_DEFAULT;
	private BigDecimal irpf = IRPF_DEFAULT;

	private FichaFinanceiraBuilder() {
	}
	
	public static FichaFinanceiraBuilder umaFichaFinanceira() {
		return new FichaFinanceiraBuilder();
	}
	
	public FichaFinanceiraBuilder doFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
		return this;
	}
	
	public FichaFinanceiraBuilder comMesAnoDeReferencia(LocalDate mesAnoReferencia) {
		this.mesAnoReferencia = mesAnoReferencia;
		return this;
	}
	
	public FichaFinanceiraBuilder dataDePagamentoEm(LocalDate dataPagamento) {
		this.dataPagamento = dataPagamento;
		return this;
	}
	
	public FichaFinanceiraBuilder comSalarioBrutoDe(BigDecimal salarioBruto) {
		this.salarioBruto = salarioBruto;
		return this;
	}
	
	public FichaFinanceiraBuilder eIrpfFinalDe(BigDecimal irpf) {
		this.irpf = irpf;
		return this;
	}
	
	public FichaFinanceira build() {
		FichaFinanceira ficha = new FichaFinanceira();
		ficha.setFuncionario(funcionario);
		ficha.setMesAnoReferencia(mesAnoReferencia);
		ficha.setDataPagamento(dataPagamento);
		ficha.setSalarioBruto(salarioBruto);
		ficha.setIrpf(irpf);
		return ficha;
	}
}
