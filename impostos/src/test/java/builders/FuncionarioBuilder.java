package builders;

import java.time.LocalDate;

import br.ucsal.testequalidade.impostos.domain.Funcionario;

public class FuncionarioBuilder {
	
	private static final Integer MATRICULA_DEFAULT = 1234;
	private static final String NOME_DEFAULT = "Jo�o Guilherme" ;
	private static final String ENDERECO_DEFAULT = "S�o Paulo";
	private static final String EMAIL_DEFAULT = "joao12@gmail.com";
	private static final LocalDate DATA_NASCIMENTO_DEFAULT = LocalDate.of(2001, 07, 13);
	private static final Boolean POSSUI_DOENCA_GRAVE_DEFAULT = false;
	
	private Integer matricula = MATRICULA_DEFAULT;
	private String nome = NOME_DEFAULT;
	private String endereco = ENDERECO_DEFAULT;
	private String email = EMAIL_DEFAULT;
	private LocalDate dataNascimento = DATA_NASCIMENTO_DEFAULT;
	private Boolean isPossuiDoencaGrave = POSSUI_DOENCA_GRAVE_DEFAULT;
	
	private FuncionarioBuilder() {	
	}
	
	public static FuncionarioBuilder umFuncionario() {
		return new FuncionarioBuilder();
	}
	
	public FuncionarioBuilder comMatricula(Integer matricula) {
		this.matricula = matricula;
		return this;
	}

	public FuncionarioBuilder nome(String nome) {
		this.nome = nome;
		return this;
	}
	
	public FuncionarioBuilder endereco(String endereco) {
		this.endereco = endereco;
		return this;
	}
	
	public FuncionarioBuilder eEmail(String email) {
		this.email = email;
		return this;
	}
	
	public FuncionarioBuilder nascidoEm(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
		return this;
	}

	public FuncionarioBuilder ePortadorOuNaoDeDoencasGraves(Boolean isPossuiDoencaGrave) {
		this.isPossuiDoencaGrave = isPossuiDoencaGrave;
		return this;
	}

	public Funcionario build() {
		Funcionario funcionario = new Funcionario();
		funcionario.setMatricula(matricula);
		funcionario.setNome(nome);
		funcionario.setEndereco(endereco);
		funcionario.setEmail(email);
		funcionario.setDataNascimento(dataNascimento);
		funcionario.setIsPossuiDoencaGrave(isPossuiDoencaGrave);
		return funcionario;
	}
}

